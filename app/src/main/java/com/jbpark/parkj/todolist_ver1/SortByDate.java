package com.jbpark.parkj.todolist_ver1;

import java.util.Comparator;

/**
 * Created by parkj on 5/26/2015.
 */
public class SortByDate implements Comparator<ToDoItem> {
    @Override
    public int compare(ToDoItem lhs, ToDoItem rhs) {
        return lhs.getDate().compareTo(rhs.getDate());
    }
}