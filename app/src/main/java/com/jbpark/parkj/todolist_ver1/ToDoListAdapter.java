package com.jbpark.parkj.todolist_ver1;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ToDoListAdapter extends BaseAdapter {

    private final List<ToDoItem> mItems = new ArrayList<ToDoItem>();
    private final Context mContext;

    private static final String TAG = "Lab-UserInterface";
    private static SortByDate sortdate = new SortByDate();
    private static SortByPriority sortpriority = new SortByPriority();

    public ToDoListAdapter(Context context) {

        mContext = context;

    }

    // Add a ToDoItem to the adapter
    // Notify observers that the data set has changed

    public void add(ToDoItem item, int page_position) {

        mItems.add(item);
        sortItems(page_position);
        notifyDataSetChanged();
    }

    // Clears the list adapter of all items.

    public void clear() {

        mItems.clear();
        notifyDataSetChanged();

    }

    public void removeItem(int pos) {

        mItems.remove(pos);
        notifyDataSetChanged();

    }

    public void editItem(ToDoItem item, int pos) {

        mItems.set(pos, item);
        sortItems(MainActivity.fragmentPagerAdapter.getPosition());
        notifyDataSetChanged();

    }

    public void sortItems(int position){
        if (position == 0){
            Collections.sort(mItems, sortdate);
        }else{
            Collections.sort(mItems, sortdate);
            Collections.sort(mItems, Collections.reverseOrder(sortpriority));
        }
    }

    // Returns the number of ToDoItems

    @Override
    public int getCount() {

        return mItems.size();

    }

    // Retrieve the number of ToDoItems

    @Override
    public Object getItem(int pos) {

        return mItems.get(pos);

    }




    // Get the ID for the ToDoItem
    // In this case it's just the position

    @Override
    public long getItemId(int pos) {

        return pos;

    }

    // Create a View for the ToDoItem at specified position
    // Remember to check whether convertView holds an already allocated View
    // before created a new View.
    // Consider using the ViewHolder pattern to make scrolling more efficient
    // See: http://developer.android.com/training/improving-layouts/smooth-scrolling.html

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        //TODO - Get the current ToDoItem
        final ToDoItem toDoItem = mItems.get(position);

        //TODO - Inflate the View for this ToDoItem
        // from todo_item.xml.
        RelativeLayout itemLayout = (RelativeLayout) convertView;

        if (convertView == null){
            itemLayout = (RelativeLayout) LayoutInflater
                    .from(mContext)
                    .inflate(R.layout.todo_item, parent, false);
        }

        //TODO - Fill in specific ToDoItem data
        // Remember that the data that goes in this View
        // corresponds to the user interface elements defined
        // in the layout file

        //TODO - Display Title in TextView

        final TextView titleView = (TextView) itemLayout.findViewById(R.id.titleView);
        titleView.setText(toDoItem.getTitle());

        // TODO - Set up Status CheckBox

        final CheckBox statusView = (CheckBox) itemLayout.findViewById(R.id.statusCheckBox);
        statusView.setChecked(toDoItem.getStatus().equals(ToDoItem.Status.DONE));


        statusView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                // TODO - Set up and implement an OnCheckedChangeListener, which
                // is called when the user toggles the status checkbox
                if (isChecked) {
                    toDoItem.setStatus(ToDoItem.Status.DONE);
                } else {
                    toDoItem.setStatus(ToDoItem.Status.NOTDONE);
                }
                MainActivity.mAdapter.notifyDataSetChanged();
            }
        });

        //TODO - Display Priority in a TextView

        final TextView priorityView = (TextView) itemLayout.findViewById(R.id.priorityView);
        priorityView.setText(toDoItem.getPriority().toString());


        // TODO - Display Time and Date.
        // Hint - use ToDoItem.FORMAT.format(toDoItem.getDate()) to get date and time String

        final TextView dateView = (TextView) itemLayout.findViewById(R.id.dateView);
        dateView.setText(ToDoItem.FORMAT.format(toDoItem.getDate()));

        // Return the View you just created
        return itemLayout;

    }
}
