package com.jbpark.parkj.todolist_ver1;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class Fragment2 extends Fragment {

    private DialogFragment mDialog;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment2, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ListView lv_priority = (ListView) getActivity().findViewById(R.id.lv_priority);
        lv_priority.setFooterDividersEnabled(true);

        ColorDrawable divcolor = new ColorDrawable(Color.DKGRAY);
        lv_priority.setDivider(divcolor);
        lv_priority.setDividerHeight(1);


        TextView footerView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.footer_view, lv_priority, false);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO - Implement OnClick().
                Intent new_item = new Intent(getActivity(), AddToDoActivity.class);
                new_item.putExtra("page_position", 1);
                getActivity().startActivityForResult(new_item, MainActivity.ADD_TODO_ITEM_REQUEST);
            }
        });

        lv_priority.addFooterView(footerView);

        lv_priority.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Intent i = new Intent(getActivity(), AddToDoActivity.class);
                i.putExtra("title", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getTitle());
                i.putExtra("priority", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getPriority().toString());
                i.putExtra("status", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getStatus().toString());
                i.putExtra("date", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getDate().toString());
                i.putExtra("edit", true);
                i.putExtra("position", position);
                getActivity().startActivityForResult(i, MainActivity.EDIT_TODO_ITEM_REQUEST);
            }
        });

        lv_priority.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mDialog = DeleteDialogFragment.newInstance(position, true);
                mDialog.show(getActivity().getFragmentManager(), "Delete");
                return true;
            }
        });

        lv_priority.setAdapter(MainActivity.mAdapter);
    }
}