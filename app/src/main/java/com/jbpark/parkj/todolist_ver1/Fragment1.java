package com.jbpark.parkj.todolist_ver1;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

public class Fragment1 extends Fragment {

    private DialogFragment mDialog;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment1, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ListView lv_date = (ListView) getActivity().findViewById(R.id.lv_date);
        lv_date.setFooterDividersEnabled(true);

        ColorDrawable divcolor = new ColorDrawable(Color.DKGRAY);
        lv_date.setDivider(divcolor);
        lv_date.setDividerHeight(1);


        TextView footerView = (TextView) getActivity().getLayoutInflater().inflate(R.layout.footer_view, lv_date, false);
        footerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //TODO - Implement OnClick().
                Intent new_item = new Intent(getActivity(), AddToDoActivity.class);
                new_item.putExtra("page_position", 0);
                getActivity().startActivityForResult(new_item, MainActivity.ADD_TODO_ITEM_REQUEST);
            }
        });

        lv_date.addFooterView(footerView);

        lv_date.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                Intent i = new Intent(getActivity(), AddToDoActivity.class);
                i.putExtra("title", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getTitle());
                i.putExtra("priority", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getPriority().toString());
                i.putExtra("status", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getStatus().toString());
                i.putExtra("date", ((ToDoItem) (MainActivity.mAdapter.getItem(position))).getDate().toString());
                i.putExtra("edit", true);
                i.putExtra("position", position);
                getActivity().startActivityForResult(i, MainActivity.EDIT_TODO_ITEM_REQUEST);
            }
        });

        lv_date.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                mDialog = DeleteDialogFragment.newInstance(position, true);
                mDialog.show(getActivity().getFragmentManager(), "Delete");
                return true;
            }
        });

        lv_date.setAdapter(MainActivity.mAdapter);
    }
}