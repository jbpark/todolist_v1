package com.jbpark.parkj.todolist_ver1;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by parkj on 5/26/2015.
 */
public class DeleteDialogFragment extends DialogFragment {
    public static DeleteDialogFragment newInstance(int position, boolean frag1){
        DeleteDialogFragment f = new DeleteDialogFragment();
        Bundle b = new Bundle();
        b.putInt("position", position);
        f.setArguments(b);
        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setMessage("Do you really want to delete To-Do Item?")

                        // User cannot dismiss dialog by hitting back button
                .setCancelable(false)

                        // Set up No Button
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int id) {
                                dismiss();
                            }
                        })

                        // Set up Yes Button
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(
                                    final DialogInterface dialog, int id) {
                                ( (MainActivity)getActivity() ).remove(getArguments().getInt("position"));
                            }
                        }).create();
    }
}