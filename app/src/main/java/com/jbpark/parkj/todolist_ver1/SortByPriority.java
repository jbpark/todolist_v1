package com.jbpark.parkj.todolist_ver1;

import java.util.Comparator;

/**
 * Created by parkj on 5/26/2015.
 */
public class SortByPriority implements Comparator<ToDoItem>{

    @Override
    public int compare(ToDoItem lhs, ToDoItem rhs) {
        return lhs.getPriority().compareTo(rhs.getPriority());
        //return lhs.getDate().compareTo(rhs.getDate());
    }
}
