package com.jbpark.parkj.todolist_ver1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;

import com.jbpark.parkj.todolist_ver1.ToDoItem.Priority;
import com.jbpark.parkj.todolist_ver1.ToDoItem.Status;

public class MainActivity extends ActionBarActivity {

    private ViewPager mPager;
    ActionBar mActionbar;
    private static final String FILE_NAME_1 = "TodoManagerActivityData.txt";
    public static ToDoListAdapter mAdapter;
    public static MyFragmentPagerAdapter fragmentPagerAdapter;
    private static final int MENU_DELETE = Menu.FIRST;
    private static final int MENU_DUMP = Menu.FIRST + 1;
    public static final int ADD_TODO_ITEM_REQUEST = 0;
    public static final int EDIT_TODO_ITEM_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAdapter = new ToDoListAdapter(getApplicationContext());
        /** Getting a reference to action bar of this activity */
        mActionbar = getSupportActionBar();

        /** Set tab navigation mode */
        mActionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        /** Getting a reference to ViewPager from the layout */
        mPager = (ViewPager) findViewById(R.id.pager);

        /** Getting a reference to FragmentManager */
        FragmentManager fm = getSupportFragmentManager();

        /** Creating an instance of FragmentPagerAdapter */
        fragmentPagerAdapter = new MyFragmentPagerAdapter(fm, getApplicationContext(), 0);

        /** Defining a listener for pageChange */
        ViewPager.SimpleOnPageChangeListener pageChangeListener = new ViewPager.SimpleOnPageChangeListener(){
            @Override
            public void onPageSelected(int position) {
                mPager.setCurrentItem(position);
                /*Toast.makeText(getApplicationContext(), "" + position,
                        Toast.LENGTH_SHORT).show();*/
                mAdapter.sortItems(position);
                mAdapter.notifyDataSetChanged();
                mActionbar.setSelectedNavigationItem(position);
                fragmentPagerAdapter.setPosition(position);
            }
        };


        /** Setting the pageChange listener to the viewPager */
        mPager.setOnPageChangeListener(pageChangeListener);


        /** Setting the FragmentPagerAdapter object to the viewPager object */
        mPager.setAdapter(fragmentPagerAdapter);

        mActionbar.setDisplayShowTitleEnabled(true);

        /** Defining tab listener */
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {

            @Override
            public void onTabUnselected(Tab tab, FragmentTransaction ft) {
            }

            @Override
            public void onTabSelected(Tab tab, FragmentTransaction ft) {
                mPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabReselected(Tab tab, FragmentTransaction ft) {
            }
        };

        /** Creating fragment1 Tab */
        Tab tab = mActionbar.newTab()
                .setText("Sort by Date")
                .setTabListener(tabListener);

        mActionbar.addTab(tab);

        /** Creating fragment2 Tab */
        tab = mActionbar.newTab()
                .setText("Sort by Priority")
                .setTabListener(tabListener);
        mActionbar.addTab(tab);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO - Check result code and request code
        // if user submitted a new ToDoItem
        // Create a new ToDoItem from the data Intent
        // and then add it to the adapter
        if (requestCode == ADD_TODO_ITEM_REQUEST) {
            switch (resultCode) {
                case RESULT_OK:
                    ToDoItem newTodoItem = new ToDoItem(data);
                    int page_position = data.getIntExtra("page_position", 0);
                    mAdapter.add(newTodoItem, page_position);
                    break;
                case RESULT_CANCELED:
                    break;
            }
        }

        if (requestCode == EDIT_TODO_ITEM_REQUEST){
            switch (resultCode){
                case RESULT_OK:
                    int position = data.getIntExtra("position", -1);
                    ToDoItem newTodoItem = new ToDoItem(data);
                    mAdapter.editItem(newTodoItem, position);

                    break;
                case RESULT_CANCELED:
                    break;
            }
        }
    }

    public void remove(int position){
        mAdapter.removeItem(position);
    }

    @Override
    public void onPause() {

        super.onPause();

        saveItems();
    }

    @Override
    public void onResume() {

        super.onResume();
        if (mAdapter.getCount() == 0)
            loadItems();


    }
    // Load stored ToDoItems
    private void loadItems() {
        BufferedReader reader = null;
        try {
            FileInputStream fis = openFileInput(FILE_NAME_1);
            reader = new BufferedReader(new InputStreamReader(fis));

            String title;
            String priority;
            String status;
            Date date;

            while (null != (title = reader.readLine())) {
                priority = reader.readLine();
                status = reader.readLine();
                date = ToDoItem.FORMAT.parse(reader.readLine());
                mAdapter.add(new ToDoItem(title, Priority.valueOf(priority),
                        Status.valueOf(status), date), 0);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            if (null != reader) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }



    // Save ToDoItems to file
    private void saveItems() {
        PrintWriter writer = null;
        try {
            FileOutputStream fos = openFileOutput(FILE_NAME_1, MODE_PRIVATE);
            writer = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    fos)));

            for (int idx = 0; idx < mAdapter.getCount(); idx++) {

                writer.println(mAdapter.getItem(idx));

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != writer) {
                writer.close();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        menu.add(Menu.NONE, MENU_DELETE, Menu.NONE, "Delete all");
        menu.add(Menu.NONE, MENU_DUMP, Menu.NONE, "Dump to log");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_DELETE:
                mAdapter.clear();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}