package com.jbpark.parkj.todolist_ver1;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Toast;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter{

    final int PAGE_COUNT = 2;
    private final Context mContext;
    private int mPosition = 0;

    /** Constructor of the class */
    public MyFragmentPagerAdapter(FragmentManager fm, Context context, int position) {
        super(fm);
        mContext = context;
        this.mPosition = position;
    }

    /** This method will be invoked when a page is requested to create */
    @Override
    public Fragment getItem(int arg0) {
        Bundle data = new Bundle();
        switch(arg0){
            /** tab1 is selected */
            case 0:
                Fragment1 fragment1 = new Fragment1();
                return fragment1;

            /** tab2 is selected */
            case 1:
                Fragment2 fragment2 = new Fragment2();
                return fragment2;
        }
        return null;
    }

    public void setPosition(int position){
        this.mPosition = position;
    }

    public int getPosition(){
        return mPosition;
    }

    /** Returns the number of pages */
    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}